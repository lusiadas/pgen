function pgen_instructions
  set -l bld (set_color 00afff -o)
  set -l reg (set_color normal)
  echo $bld"pgen

"$bld"DESCRIPTION

"$bld"Passphrase Generator."$reg" Generate or increment password encrypted wordlists from which to retrieve passphrases. The default wordlist is \"EFF's long word list\", which can be supplemented or substitued with additional words or wordlists drawn from Wordnik's (https://wordnik.com) dictionary. The general idea for this plugin is for every user to generate their own original and encrypted wordlist from which to build their own passphrases.

"$bld"OPTIONS

"$bld"-r/--retrieve"$reg" [-c/--clipboard] [QUANTITY] [WORDLIST]

Retrieve passphrases from a wordlist. This is the default option. If no quantity of word is described, 6 words are drawn. If no wordlist is described, "(realpath (dirname (status -f))/../default.cpt)" is used. If the wordlist hasn't enough words to build the requested passphrase, words shall be added to the wordlist (see option \"-a/--add\").

To have the resulting passphrase sent directly to your clipboard, add the modifier flag "$bld"-c/--clipboard"$reg". Whenever possible, though, this should be avoided: many processes, even if sandboxed, have access to the contents of the clipboard. So, It is preferable to pipe the result of this function directly to a password store.

"$bld"-a/--add|-t/--trim|-o/--overwrite"$reg" [-w/--without-tor] [QUANTITY] [WORDLIST]

Add words to, or trim words from, or overwrite the contents of, a wordlist. If no subcommand is passed "$bld"-a/--add"$reg" is assumed, and if no quantity is described, 6 words are assumed. The process can be interrupted, and its progress saved using "$bld"^Z"$reg" (i.e. Ctrl+Z).

Words are draw through the tor network. This is a slower method, but one that protects the confidentiality of the process from traffic analysis. To draw words faster, you can disable it by accessing Wordnik's random word page directly with "$bld"-w/--without-tor"$reg".

"$bld"-p/--password"$reg" [PASSWORD]

Used with "$bld"-r/--retrieve"$reg", "$bld"-a/--add"$reg", "$bld"-t/--trim"$reg", or "$bld"-o/--overwrite"$reg" it skips the password dialog by passing the password directly. By specifying the keyword as \"-\", input will be read from stdin before assuming your password really is \"-\". Never type in your password, as it'll be visible in the command line and later its history as well. Pass passwords either though a subcommand or though a pipe, "$bld" but be sure that the password passed is correct, or the file's data will be corrupted.

"$bld"-n/--new-password"$reg" [PASSWORD] [FILE]

Change the password of an encryped wordlist.

"$bld"-h/--help

Display these instructions

"$bld"NOTE

Wordlists whose names include only digits must be described by their relative or absolute path. "$bld"i.e."$reg" \"./999\" will target a wordlist in the current folder named \"999\".
" | less -R
end
